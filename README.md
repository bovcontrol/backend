The candidates must create an account in hacker ranck, www.hackerrank.com, and solve the problems selected by our team.

Algorithms* 
https://www.hackerrank.com/challenges/cavity-map

https://www.hackerrank.com/challenges/lilys-homework

https://www.hackerrank.com/challenges/non-divisible-subset

https://www.hackerrank.com/challenges/new-year-chaos

https://www.hackerrank.com/challenges/bomber-man

Solve the above problems in PHP, node JS or Python 


SQL*
https://www.hackerrank.com/challenges/15-days-of-learning-sql

https://www.hackerrank.com/challenges/occupations

https://www.hackerrank.com/challenges/the-pads

https://www.hackerrank.com/challenges/weather-observation-station-20

*All candidates must use MySQL.

The candidate should send the code developed to solve all the  exercises in a single PDF  document as follows:

Algorithms - 1:

import java.io.*;

import java.util.*;

import java.text.*;

import java.math.*;

import java.util.regex.*;


public class Solution {

    static int solveMeFirst(int a, int b) {
    
      return a+b;
      
   }
   

 public static void main(String[] args) {
 
        Scanner in = new Scanner(System.in);
        
        int a;
        
        a = in.nextInt();
        
        int b;
        
        b = in.nextInt();
        
        int sum;
        
        sum = solveMeFirst(a, b);
        
        System.out.println(sum);
        
   }
   
}


Algorithms - 2:

// Code goes here

*** Submission

The candidate must implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1.Candidate will fork this repository (will not clone directly!)

2.It will make your project on that fork.

3.Commit and upload changes to your fork.

4.Through the Bitbucket interface, you will send a Pull Request.

5.If possible to leave the public fork to facilitate inspection of the code

